﻿using System;

namespace CompProjectGUIClient
{
	public partial class LoginWindow : Gtk.Window
	{
		public delegate void LoginWindowDelegate(string username, string password);
		public event LoginWindowDelegate OnOK;
		public event LoginWindowDelegate OnCancel;

		public LoginWindow () :
			base (Gtk.WindowType.Toplevel)
		{
			this.Build ();
			cancel_button.Clicked += (object sender, EventArgs e) => {
				OnCancel("","");
			};
			ok_button.Clicked += (object sender, EventArgs e) => {
				OnOK(username_textBox.Text,password_textBox.Text);
			};
		}
	}
}

