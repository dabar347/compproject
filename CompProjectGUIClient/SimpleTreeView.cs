﻿using System;
using System.Collections.Generic;
using Gtk;

namespace CompProjectGUIClient
{
	public class SimpleTreeView{
		List<Gtk.TreeViewColumn> _cols;
		List<Gtk.CellRendererText> _cells;
		Gtk.TreeView _tree;
		Gtk.ListStore _list=null;
		bool _colsAdded=false;

		public SimpleTreeView(Gtk.TreeView tree){
			_tree=tree;
			_cols = new List<Gtk.TreeViewColumn> ();
			_cells=new List<Gtk.CellRendererText>();
		}

		public void Clear()
		{
			_cols = new List<Gtk.TreeViewColumn> (_tree.Columns);
		}

		public void AddColumn(string colName){
			if(_colsAdded == true){
				// can't add columns after you've started adding data.
				throw new Exception("Cannot add columns after data has been added");
			}
			_cols.Add(new Gtk.TreeViewColumn()); 
			_cols[_cols.Count - 1].Title = colName;
		}

		public void AddData(params string[] fieldData){
			// it is assumed when this is called that the user has finished adding columns.
			_colsAdded = true;
			if(_cols.Count != fieldData.Length){
				throw new Exception("Mismatch on number of columns defined and items of data passed");
			}

			if(_list == null){
				Type[] t = new Type[_cols.Count];
				for(int n=0; n<_cols.Count; ++n){
					t[n] = typeof(string);
				}
				_list = new Gtk.ListStore(t);
			}
			_list.AppendValues(fieldData);
		}

		public void Finish(){
			for(int n=0; n<_cols.Count; ++n){
				_cells.Add(new Gtk.CellRendererText());
				_cols[n].PackStart(_cells[_cells.Count-1], true);
			}
			for(int n=0; n<_cols.Count; ++n){
				_cols[n].AddAttribute(_cells[n], "text", n);
			}
//			_tree
			for(int n=0; n<_cols.Count; ++n){
				_tree.AppendColumn(_cols[n]);
			}
			_tree.Model=_list;
		}

		public List<string> GetSelected(){
			// returns the cols of the current line
			Gtk.TreeSelection selection =_tree.Selection;
			Gtk.TreeModel model;
			Gtk.TreeIter iter;
			List<string> rc=new List<string>();
			if(selection.GetSelected(out model, out iter)){
				for(int n=0; n<_cols.Count; n++){
					rc.Add(model.GetValue(iter,n).ToString());
				}
			}
			return rc;
		}
	}
}

