﻿using System;
using System.Threading;
using Gtk;
using CompProjectGUIClient;
using CompProjectCryptorLib;
using agsXMPP;
using agsXMPP.protocol.client;
using agsXMPP.Xml.Dom;

public partial class MainWindow: Gtk.Window
{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();

		//Draw contacts_tree template
		SimpleTreeView contacts_tree = new SimpleTreeView (contacts_treeview);
		contacts_tree.AddColumn ("JIDs");
		contacts_tree.Finish ();
		contacts_treeview.Model = PrepareModel (contacts_treeview);

		//Draw messages_tree template
		SimpleTreeView messages_tree = new SimpleTreeView (messages_treeview);
		messages_tree.AddColumn ("X");
		messages_tree.AddColumn ("Time");
		messages_tree.AddColumn ("Message");
		messages_tree.Finish ();
		messages_treeview.Model = PrepareModel (messages_treeview);
		
		//Generate RSA keys in an independant thread, set public key as the XMPP status and show message if successed
		Thread RSAThread = new Thread (() => {
			Handler.cryptor.GenerateRSAKeys();
			Handler.xmppClient.Status = Handler.cryptor.publicRSAKey.ToString();
			Handler.xmppClient.SendMyPresence();
			SetStatusBar("RSA key has been generated");
		});
		RSAThread.Start ();

		//Reload message tree when different contact chosen
		contacts_treeview.CursorChanged += (object sender, EventArgs e) => {
			messages_treeview.Model = PrepareModel (messages_treeview);
			TreeIter iter;
			TreeModel model;
			if(contacts_treeview.Selection.GetSelected(out model, out iter))
			{
				String to = contacts_treeview.Model.GetValue(iter,0).ToString();
				if (Handler.messagesDictionary.ContainsKey(to))
				{
					foreach (var v in Handler.messagesDictionary[to])
					{
						string[] array = new string[]{v.mine ? "Sent" : "Received", v.time.ToShortTimeString(),v.message};
						(messages_treeview.Model as ListStore).AppendValues(array);
					}
				}
			}
		};

		//Add listeners to xmppClient events in order to get Roster/Contact list
		Handler.xmppClient.OnRosterStart += (object sender) => {
			Handler.rosterList.Clear();
		};

		Handler.xmppClient.OnRosterItem += (object sender, agsXMPP.protocol.iq.roster.RosterItem item) => 
		{
			Handler.rosterList.Add(item);
		};

		Handler.xmppClient.OnRosterEnd += (object sender) => {
			Type[] t = new Type[contacts_treeview.Columns.Length];
			for(int n=0; n<contacts_treeview.Columns.Length; ++n){
				t[n] = typeof(string);
			}
			ListStore model = PrepareModel(contacts_treeview);
			foreach (var v in Handler.rosterList)
			{
				model.AppendValues(v.Jid.ToString());
			}
			contacts_treeview.Model = model;
		};

		//Listener of xmppClient event in order to get status messages from other users and hence their public keys
		Handler.xmppClient.OnPresence += (object sender, Presence pres) => {
			Handler.statusDictionary[pres.From.Bare] = pres.Status;
		};

		//Listener of xmppClient event in order to get recieve messages and show them if required
		Handler.xmppClient.OnMessage += (object sender, Message msg) => 
		{
			try
			{
				Packet recievedPacket = new Packet();
				recievedPacket.FromString(msg.Body);
				String decryptedMsg = Handler.cryptor.DecryptPacket(recievedPacket);
				if (!Handler.messagesDictionary.ContainsKey(msg.From.Bare))
				{
					Handler.messagesDictionary.Add(msg.From.Bare,new System.Collections.Generic.List<MessageEntry>());
				}
				MessageEntry messageEntry = new MessageEntry(false,DateTime.Now,decryptedMsg);
				Handler.messagesDictionary[msg.From.Bare].Add(messageEntry);
				SetStatusBar("Received message");
				TreeIter iter;
				TreeModel model;
				if(contacts_treeview.Selection.GetSelected(out model, out iter))
				{
					String from = contacts_treeview.Model.GetValue(iter,0).ToString();
					if (from == msg.From.Bare)
					{
						string[] array = new string[]{messageEntry.mine ? "Sent" : "Received", messageEntry.time.ToShortTimeString(),messageEntry.message};
						(messages_treeview.Model as ListStore).AppendValues(array);
					}
				}
			}
			catch
			{	
				SetStatusBar("Received corrupted or not crypted message");
			}

		};

		//Log In button in menu clicked
		LogInAction.Activated += (object sender, EventArgs e) => 
		{
			if (Handler.xmppClient.XmppConnectionState == agsXMPP.XmppConnectionState.Disconnected)
			{
				if (!Handler.cryptor.privateRSAKey.IsEmpty()){
					LoginWindow window = new LoginWindow();
					window.Title = "Login";
					window.OnCancel += (string username, string password) => {
						window.Destroy();
					};
					window.OnOK += (string username, string password) => {
						window.Destroy();
						if (username != "" && password != "")
						{
							string[] splitted = username.Split('@');
							if (splitted.Length != 2)
							{
								SetStatusBar("Username must be in form login@hostname");
							}
							else
							{
								SetStatusBar("Trying to log in");
								Handler.xmppClient.RegisterAccount = false;
								Handler.xmppClient.OnLogin += OnLogin;
								Handler.xmppClient.OnAuthError += OnAuthError;
								Handler.xmppClient.Server = splitted[1];
								Handler.xmppClient.Open(splitted[0],password);
							}
						}
						else
						{
							SetStatusBar("Username and password must not be empty");
						}
					};
					window.Show();
				}
				else
				{
					SetStatusBar("RSA key must be generated first");
				}
			}
			else
			{
				SetStatusBar("You should disconnect first");
			}
		};

		//Registration button in menu clicked
		RegisterAction.Activated += (object sender, EventArgs e) => {
			if (Handler.xmppClient.XmppConnectionState == agsXMPP.XmppConnectionState.Disconnected)
			{
				if (!Handler.cryptor.privateRSAKey.IsEmpty()){
					LoginWindow window = new LoginWindow();
					window.Title = "Registration";
					window.OnCancel += (string username, string password) => {
						window.Destroy();
					};
					window.OnOK += (string username, string password) => {
						window.Destroy();
						if (username != "" && password != "")
						{
							string[] splitted = username.Split('@');
							if (splitted.Length != 2)
							{
								SetStatusBar("Username must be in form login@hostname");
							}
							else
							{
								SetStatusBar("Trying to register and log in");
								Handler.xmppClient.RegisterAccount = true;
								Handler.xmppClient.OnLogin += OnLogin;
								Handler.xmppClient.OnAuthError += OnAuthError;
								Handler.xmppClient.Server = splitted[1];
								Handler.xmppClient.Open(splitted[0],password);
							}
						}
						else
						{
							SetStatusBar("Username and password must not be empty");
						}
					};
					window.Show();
				}
				else
				{
					SetStatusBar("RSA key must be generated first");
				}
			}
			else
			{
				SetStatusBar("You should disconnect first");
			}
		};
			
		//Log Out button in menu clicked
		LogOutAction.Activated += (object sender, EventArgs e) => {
			Handler.xmppClient.OnClose += OnClose;
			Handler.xmppClient.Close();
		};

		//Send button clicked
		send_button.Clicked += (object sender, EventArgs e) => {
			TreeIter iter;
			TreeModel model;
			if(contacts_treeview.Selection.GetSelected(out model, out iter))
			{
				String to = contacts_treeview.Model.GetValue(iter,0).ToString();
				RSAKey key = new RSAKey();
				if (Handler.statusDictionary.ContainsKey(to))
					key.FromString (Handler.statusDictionary[to]);
				if (!key.IsEmpty())
				{
					Handler.xmppClient.Send(new Message(
						Handler.GetRosterItemWithString(to).Jid.ToString()+"/agsXMPP",
						agsXMPP.protocol.client.MessageType.chat,
						Handler.cryptor.EncryptPacket(key,message_entry.Text).ToString()));						
					SetStatusBar("Message is sent");
					if (!Handler.messagesDictionary.ContainsKey(to))
					{
						Handler.messagesDictionary.Add(to,new System.Collections.Generic.List<MessageEntry>());
					}
					MessageEntry messageEntry = new MessageEntry(true,DateTime.Now,message_entry.Text);
					Handler.messagesDictionary[to].Add(messageEntry);
					string[] array = new string[]{messageEntry.mine ? "Sent" : "Received", messageEntry.time.ToShortTimeString(),messageEntry.message};
					(messages_treeview.Model as ListStore).AppendValues(array);
					message_entry.Text = @"";
										
				}
				else
				{
					SetStatusBar("Recipient cannot handle encrypted messages");
				}
			}
		};
	}

	private ListStore PrepareModel(TreeView treeview)
	{
		Type[] t = new Type[treeview.Columns.Length];
		for(int n=0; n<treeview.Columns.Length; ++n){
			t[n] = typeof(string);
		}
		return new ListStore(t);
	}

	private void OnClose (object sender)
	{
		Handler.xmppClient.OnClose -= OnClose;
		Handler.rosterList.Clear ();
		Handler.messagesDictionary.Clear ();
		Handler.statusDictionary.Clear ();
		contacts_treeview.Model = PrepareModel(contacts_treeview);
		messages_treeview.Model = PrepareModel (messages_treeview);
	}

	private void OnLogin (object sender)
	{
		SetStatusBar("Loged in successfully");
		Handler.xmppClient.OnLogin -= OnLogin;
		Handler.xmppClient.OnAuthError -= OnAuthError;
	}

	private void OnAuthError (object sender, Element ex)
	{
		SetStatusBar("Wrong username or password");
		Handler.xmppClient.OnLogin -= OnLogin;
		Handler.xmppClient.OnAuthError -= OnAuthError;
		Handler.xmppClient.OnClose += OnClose;
		Handler.xmppClient.Close ();
	}

	private void SetStatusBar(string text)
	{
		statusbar.Push(this.statusbar.GetContextId(""),text);
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
}
