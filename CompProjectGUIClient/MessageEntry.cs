﻿using System;

namespace CompProjectGUIClient
{
	public class MessageEntry
	{
		public bool mine;
		public DateTime time;
		public String message;

		public MessageEntry (bool mine, DateTime time, String message)
		{
			this.mine = mine;
			this.time = time;
			this.message = message;
		}
	}
}

