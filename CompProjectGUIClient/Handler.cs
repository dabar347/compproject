﻿using System;
using System.Collections.Generic;

using agsXMPP;
using agsXMPP.protocol.iq.roster;
using CompProjectCryptorLib;

namespace CompProjectGUIClient
{
	public static class Handler
	{
		public static readonly XmppClientConnection xmppClient = new XmppClientConnection();
		public static readonly List<RosterItem> rosterList = new List<RosterItem>();
		public static readonly Cryptor cryptor = new Cryptor ();
		public static readonly Dictionary<string,string> statusDictionary = new Dictionary<string, string> ();
		public static readonly Dictionary<string,List<MessageEntry>> messagesDictionary = new Dictionary<string, List<MessageEntry>> ();

		public static RosterItem GetRosterItemWithString(string jidString)
		{
			foreach (var v in rosterList) {
				if (v.Jid.ToString () == jidString) {
					return v;
				}
			}
			return null;
		}
	
	}
}