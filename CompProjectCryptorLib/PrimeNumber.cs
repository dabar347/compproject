﻿using System;
using System.Numerics;

namespace CompProjectCryptorLib
{
    class PrimeNumber
    {
        private BigInteger _iterationTested = 0;
        private BigInteger _numberTested = 0;
        private BigInteger _number = 2;

        private BigInteger _s = 10;

        private bool _found;

        private bool _running;

        public BigInteger GetPrimeNumber ()
        {
            return _number;
        }

        public BigInteger GetTestedCount ()
        {
            return _numberTested;
        }

        public BigInteger GetTestedIterations ()
        {
            return _iterationTested;
        }

        public bool GetFoundPrime()
        {
            return _found;
        }

        public void StopEngine()
        {
            _running = false;
        }

        private bool RabinMillerTest (BigInteger r, BigInteger s)
        {
            _iterationTested = 0;

            //
            // Find D and K so equality is correct: d*2^k = r - 1
            //

            BigInteger d = r - 1;
            BigInteger k = 0;

            while (d % 2 == 0)
            {
                d = d / 2;
                k = k + 1;
            }

            for (BigInteger j = 1; j <= s; j++)
            {
                _iterationTested++;

                BigInteger a = Generator.Random (2, (r - 1));
                BigInteger x = BigInteger.ModPow (a, d, r);

                if (x != 1)
                {
                    for (BigInteger i = 0; i < (k - 1); i++)
                    {
                        if (x == _number - 1)
                        {
                            break;
                        }

                        x = BigInteger.ModPow (x, 2, _number);
                    }

                    if (x != _number - 1)
                    {
                        return false;
                    }
                }

                if (_running == false)
                {
                    return false;
                }
            }

            return true;
        }


        public void SetNumber (BigInteger num)
        {
            _number = num;
        }

        public void RabinMiller ()
        {
            _running = true;
            _found = false;

            // No negative primes
            if (_number < 1)
            {
                _number = 1;
            }

            // Two is prime
            if (_number <= 2)
            {
                return;
            }

            // Other even numbers arent primes
            if (_number % 2 == 0)
            {
                _number = _number + 1;
            }

            // First five is prime
            if (_number == 5)
            {
                _running = false;
                _found = true;

                return;
            }

            _numberTested = 0;

            while (_running)
            {
                if (RabinMillerTest (_number, _s))
                {
                    _found = true;
                    _running = false;

                    return;
                }

                // Skip number 5
                if (_number % 10 == 3)
                {
                    _number = _number + 4;
                }
                else
                {
                    _number = _number + 2;
                }

                _numberTested = _numberTested + 1;
            }
        }
    }
}
