﻿using System;
using System.Windows.Forms;

namespace EasyRSA
{
    public partial class Form2 : Form
    {
        public int BitLength = 1024;

        public Form2()
        {
            InitializeComponent();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void TextBox6TextChanged(object sender, EventArgs e)
        {
            try
            {
                BitLength = Convert.ToInt32(textBox6.Text);

                if (BitLength < 1)
                {
                    throw new Exception("Negative number.");
                }

                button1.Enabled = true;
            }
            catch (Exception)
            {
                button1.Enabled = false;
            }
        }
    }
}
