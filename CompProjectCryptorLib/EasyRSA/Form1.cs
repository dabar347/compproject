﻿using System;
using System.IO;
using System.Numerics;
using System.Threading;
using System.Windows.Forms;
using EasyRSA.Library;

namespace EasyRSA
{
    public partial class Form1 : Form
    {
        private Thread _eThread;
        private Thread _pThread;
        private Thread _qThread;

        public Form1()
        {
            InitializeComponent();
        }

        private void GenerateNewKeyPairToolStripMenuItemClick(object sender, EventArgs ea)
        {
            var form = new Form2();

            // Generate keypair
            if (form.ShowDialog() == DialogResult.OK)
            {
                //
                // Generate two random numbers
                //

                // Lets use our entropy based random generator
                Generator.Initialize(2);

                // For 1024 bit key length, we take 512 bits for first prime and 512 for second prime
                // Get same min and max
                BigInteger numMin = BigInteger.Pow(2, (form.BitLength/2) - 1);
                BigInteger numMax = BigInteger.Pow(2, (form.BitLength/2));

                // Create two prime numbers
                var p = new PrimeNumber();
                var q = new PrimeNumber();

                p.SetNumber(Generator.Random(numMin, numMin));
                q.SetNumber(Generator.Random(numMin, numMax));

                // Create threaded p and q searches
                _pThread = new Thread(p.RabinMiller);
                _qThread = new Thread(q.RabinMiller);

                // For timeout
                DateTime start = DateTime.Now;

                toolStripProgressBar1.Style = ProgressBarStyle.Marquee;
                toolStripStatusLabel2.Text = "Generating p and q prime numbers.";

                _pThread.Start();
                _qThread.Start();

                while (_pThread.IsAlive || _qThread.IsAlive)
                {
                    Application.DoEvents();

                    TimeSpan ts = DateTime.Now - start;

                    if (ts.TotalMilliseconds > (1000 * 60 * 5))
                    {
                        try
                        {
                            _pThread.Abort();
                        }
                        // ReSharper disable EmptyGeneralCatchClause
                        catch (Exception)
                        // ReSharper restore EmptyGeneralCatchClause
                        {
                        }

                        try
                        {
                            _qThread.Abort();
                        }
                        // ReSharper disable EmptyGeneralCatchClause
                        catch (Exception)
                        // ReSharper restore EmptyGeneralCatchClause
                        {
                        }

                        MessageBox.Show("Key generating error: timeout.\r\n\r\nIs your bit length too large?", "Error");

                        break;
                    }
                }


                // If we found numbers, we continue to create
                if (p.GetFoundPrime() && q.GetFoundPrime())
                {
                    toolStripStatusLabel2.Text = "Generating p and q prime numbers ...";

                    BigInteger n = p.GetPrimeNumber() * q.GetPrimeNumber();
                    BigInteger euler = (p.GetPrimeNumber() - 1) * (q.GetPrimeNumber() - 1);

                    //
                    // Find random number E
                    //

                    /*
                    toolStripStatusLabel2.Text = "Generating random number e ...";

                    BigInteger e;

                    while (true)
                    {
                        e = Generator.Random(2, euler - 1);

                        if (e % 2 == 0)
                        {
                            e = e + 1;
                        }

                        if (BigInteger.GreatestCommonDivisor(e, euler) == 1)
                        {
                            break;
                        }
                    }*/

                    toolStripStatusLabel2.Text = "Generating e prime number ...";

                    var e = new PrimeNumber();

                    while (true)
                    {
                        e.SetNumber (Generator.Random(2, euler - 1));

                        start = DateTime.Now;

                        _eThread = new Thread(e.RabinMiller);
                        _eThread.Start();

                        while (_eThread.IsAlive)
                        {
                            Application.DoEvents();

                            TimeSpan ts = DateTime.Now - start;

                            if (ts.TotalMilliseconds > (1000 * 60 * 5))
                            {
                                MessageBox.Show("Key generating error: timeout.\r\n\r\nIs your bit length too large?", "Error");

                                break;
                            }
                        }

                        if (e.GetFoundPrime() && (BigInteger.GreatestCommonDivisor(e.GetPrimeNumber(), euler) == 1))
                        {
                            break;
                        }
                    }

                    if (e.GetFoundPrime())
                    {
                        toolStripStatusLabel2.Text = "Calculating key components ...";

                        // Calculate secret exponent D as inverse number of E
                        BigInteger d = MathExtended.ModularLinearEquationSolver(e.GetPrimeNumber(), 1, euler);
                        //BigInteger d = MathExtended.ModularLinearEquationSolver(83, 1, 120);

                        // Displaying keys
                        if (d > 0)
                        {
                            // N
                            textBox6.Text = n.ToString();
                            textBox8.Text = n.ToString();

                            // E
                            textBox5.Text = e.GetPrimeNumber().ToString();

                            // D
                            textBox7.Text = d.ToString();

                            tabControl1.SelectedIndex = 2;

                            toolStripStatusLabel2.Text = "Successfully created key pair.";
                        }
                        else
                        {
                            toolStripStatusLabel2.Text = "Error: Modular equation solver fault.";

                            MessageBox.Show(
                                "Error using mathematical extensions.\r\ne = " + e + "\r\neuler = " + euler + "\r\np = " +
                                p.GetPrimeNumber() + "\r\n" + q.GetPrimeNumber(), "Error");
                        }
                    }
                }
                else
                {
                    toolStripStatusLabel2.Text = "Idle";
                }

                toolStripProgressBar1.Style = ProgressBarStyle.Continuous;
            }
        }

        private void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                _pThread.Abort();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            try
            {
                _qThread.Abort();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            try
            {
                _eThread.Abort();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            Application.Exit();
        }

        private void EncryptToolStripMenuItemClick(object sender, EventArgs ea)
        {
            try
            {
                BigInteger e = BigInteger.Parse(textBox5.Text);
                BigInteger n = BigInteger.Parse(textBox6.Text);

                if (textBox1.Text.Length > n)
                {
                    MessageBox.Show("Cannot encrypt data longer than: " + n + "\r\n\r\nUse key pair of bigger length.",
                                    "Error");

                    return;
                }

                if (textBox1.Text.Length == 0)
                {
                    return;
                }

                textBox2.Text = BigInteger.ModPow(textBox1.Text.Length, e, n).ToString();

                for (int i = 0; i < textBox1.Text.Length; i++)
                {
                    char chr = textBox1.Text[i];

                    BigInteger c = BigInteger.ModPow(Convert.ToInt32(chr), e, n);

                    if (textBox2.Text == "")
                    {
                        textBox2.Text = c.ToString();
                    }
                    else
                    {
                        textBox2.Text += " " + c.ToString();
                    }
                }

                tabControl1.SelectedIndex = 1;
            }
            catch (Exception)
            {
                MessageBox.Show("Enter valid public key components to encrypt.", "Error");
            }
        }

        private void DecryptCipherToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (textBox2.Text.Length == 0)
            {
                return;
            }

            try
            {
                BigInteger d = BigInteger.Parse(textBox7.Text);
                BigInteger n = BigInteger.Parse(textBox8.Text);

                textBox1.Text = "";

                string text = "";

                try
                {
                    string[] cipher = textBox2.Text.Split(' ');

                    BigInteger m = BigInteger.ModPow(BigInteger.Parse(cipher[0]), d, n);

                    if (cipher.Length == 1)
                    {
                        textBox1.Text = m.ToString();
                    }
                    else
                    {
                        BigInteger length = m;

                        for (int i = 1; i < cipher.Length; i++)
                        {
                            m = BigInteger.ModPow(BigInteger.Parse(cipher[i]), d, n);

                            //M = (M % (BigInteger.Pow(2, 31)));

                            char chr = Convert.ToChar(Convert.ToInt32(m.ToString()));

                            text += chr;
                        }

                        if (length == text.Length)
                        {
                            textBox1.Text = text;
                        }
                        else
                        {
                            MessageBox.Show("Error decrypting: incomplete message.", "Error");
                        }
                    }

                    tabControl1.SelectedIndex = 0;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error decrypting, invalid private key.", "Error");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Enter valid private key components to decrypt.", "Error");
            }
        }

        private void SaveKeyPairToolStripMenuItemClick(object sender, EventArgs e)
        {
            if ((textBox5.Text == "") || (textBox6.Text == "") || (textBox7.Text == "") || (textBox8.Text == ""))
            {
                MessageBox.Show("Enter your public and private keys to continue.", "Error");
                return;
            }

            saveFileDialog1.Filter = "RSA key pair (*.rkp)|*.rkp|Text file (*.txt)|*.txt";
            saveFileDialog1.Title = "Save key pair to file";
            saveFileDialog1.FileName = "";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    TextWriter tw = new StreamWriter(saveFileDialog1.FileName);

                    if (Path.GetExtension(saveFileDialog1.FileName) == ".txt")
                    {
                        tw.WriteLine(textBox5.Text + " " + textBox6.Text);
                        tw.WriteLine(textBox7.Text + " " + textBox8.Text);
                    }
                    else
                    {
                        tw.WriteLine("e = " + textBox5.Text + ";");
                        tw.WriteLine("d = " + textBox7.Text + ";");
                        tw.WriteLine("n = " + textBox6.Text + ";");
                    }

                    tw.Close();

                    MessageBox.Show("Key pair successfully saved to: " + saveFileDialog1.FileName, "Success");
                }
                catch (Exception)
                {
                    MessageBox.Show("Error saving key pair to file. ", "Error");
                }
            }
        }

        private void OpenKeyPairToolStripMenuItemClick(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "RSA key pair (*.rkp)|*.rkp|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.Title = "Open stored RSA key pair";
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bool opened = true;

                TextReader tr = new StreamReader(openFileDialog1.FileName);

                if (Path.GetExtension(openFileDialog1.FileName) == ".txt")
                {
                    try
                    {
                        string readLine = tr.ReadLine();
                        if (readLine != null)
                        {
                            string[] publicKey = readLine.Split(' ');
                            string[] privateKey = readLine.Split(' ');

                            textBox5.Text = publicKey[0];
                            textBox6.Text = publicKey[1];

                            textBox7.Text = privateKey[0];
                            textBox8.Text = privateKey[1];
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Cannot open RSA keypair: File does not contain enough data.", "Error");
                    }
                }
                else
                {
                    string line;

                    while ((line = tr.ReadLine()) != null)
                    {
                        try
                        {
                            string[] parse = line.Split('=');

                            string cmd = parse[0].Trim();

                            string num = parse[1].Trim();

                            if (num.EndsWith(";"))
                            {
                                num = num.Substring(0, num.Length - 1);
                            }
                            else
                            {
                                throw new Exception("Parse error: Missing semicolon.");
                            }

                            BigInteger test = BigInteger.Parse(num);

                            if (cmd == "e")
                            {
                                textBox5.Text = test.ToString();
                            }
                            else if (cmd == "d")
                            {
                                textBox7.Text = test.ToString();
                            }
                            else if (cmd == "n")
                            {
                                textBox6.Text = textBox8.Text = test.ToString();
                            }
                            else
                            {
                                MessageBox.Show("Cannot open RSA keypair: File is invalid.", "Error");

                                opened = false;

                                break;
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Cannot open RSA keypair: File is corrupted.", "Error");

                            opened = false;

                            break;
                        }
                    }

                    tabControl1.SelectedIndex = 2;

                    if (opened)
                    {
                        MessageBox.Show("Successfully opened RSA keypair.", "Success");
                    }
                    else
                    {
                        textBox5.Text = textBox6.Text = textBox7.Text = textBox8.Text = "";
                    }
                }

                tr.Close();
            }
        }

        private void EncryptNumberToolStripMenuItemClick(object sender, EventArgs ea)
        {
            try
            {
                BigInteger e = BigInteger.Parse(textBox5.Text);
                BigInteger n = BigInteger.Parse(textBox6.Text);

                try
                {
                    if (textBox1.Text.Length > 0)
                    {
                        BigInteger c = BigInteger.Parse(textBox1.Text);

                        if (c > n)
                        {
                            MessageBox.Show(
                                "Cannot encrypt data longer than: " + n + "\r\n\r\nUse key pair of bigger length.",
                                "Error");

                            return;
                        }

                        textBox2.Text = BigInteger.ModPow(c, e, n).ToString();

                        tabControl1.SelectedIndex = 1;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid input. Are you sure you want to encrypt a number?", "Error");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Enter valid public key components to proceed with encryption.", "Error");
            }
        }

        private void TextFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.Title = "Open plain text file";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    TextReader tr = new StreamReader(openFileDialog1.FileName);

                    textBox1.Text = tr.ReadToEnd();

                    tr.Close();

                    textBox2.Text = "";

                    tabControl1.SelectedIndex = 0;

                    MessageBox.Show("Successfully opened text file.", "Success");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot open text file.\r\n" + ex.Message, "Error");
                }
            }
        }

        private void EncryptedFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "RSA Encrypted file (*.rsa)|*.rsa";
            openFileDialog1.Title = "Open encrypted file";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    TextReader tr = new StreamReader(openFileDialog1.FileName);

                    textBox2.Text = tr.ReadToEnd();

                    tr.Close();

                    textBox1.Text = "";

                    tabControl1.SelectedIndex = 1;

                    MessageBox.Show("Successfully opened encrypted file.", "Success");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot open encrypted file.\r\n" + ex.Message);
                }
            }
        }

        private void SaveCipherToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (textBox2.Text.Length > 0)
            {
                saveFileDialog1.FileName = "";
                saveFileDialog1.Filter = "RSA Encrypted file (*.rsa)|*.rsa";
                saveFileDialog1.Title = "Save cipher";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    TextWriter tw = new StreamWriter(saveFileDialog1.FileName);

                    tw.Write(textBox2.Text);

                    tw.Close();

                    MessageBox.Show("Successfully saved current cipher.", "Success");
                }
            }
        }

        private void Form1FormClosing(object sender, FormClosingEventArgs e)
        {
            ExitToolStripMenuItemClick(new object(), new EventArgs());
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var formAbout = new About();
            formAbout.ShowDialog();
        }
    }
}