using System;
using System.Runtime;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Numerics;
using System.Threading;

namespace CompProjectCryptorLib
{

	public class RSAKey
	{
		public BigInteger k;
		public BigInteger n;

		public bool IsEmpty()
		{
			return (k == new BigInteger (0) || n == new BigInteger (0));
		}

		public override string ToString ()
		{
			String s = k.ToString()+":"+n.ToString();
			byte[] b = System.Text.Encoding.UTF8.GetBytes (s);
			String b64 = System.Convert.ToBase64String(b);
			return b64;
		}

		public void FromString(string packetString)
		{
			try
			{
				String decodedString = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String (packetString));
				String[] decodedStringsArray = decodedString.Split (':');
				this.k = BigInteger.Parse (decodedStringsArray [0]);
				this.n = BigInteger.Parse (decodedStringsArray [1]);
			}
			catch
			{
				k = new BigInteger(0);
				n = new BigInteger(0);
			}
		}
	}

	public class Packet
	{
		public BigInteger encryptedXORKey;

		public string encryptedMessage;

		public bool IsEmpty()
		{
			return (encryptedXORKey == new BigInteger (0) || encryptedMessage == "");
		}

		public override string ToString ()
		{
			String s = encryptedXORKey.ToString()+":"+System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes (encryptedMessage));
			byte[] b = System.Text.Encoding.UTF8.GetBytes (s);
			String b64 = System.Convert.ToBase64String(b);
			return b64;
		}

		public void FromString(string packetString)
		{
			try {
				String decodedString = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String (packetString));
				String[] decodedStringsArray = decodedString.Split (':');
				this.encryptedXORKey = BigInteger.Parse (decodedStringsArray [0]);
				this.encryptedMessage = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String (decodedStringsArray [1]));
			}
			catch {
				this.encryptedMessage = "";
				this.encryptedXORKey = new BigInteger (0);
			}
		}
	}

	public class Cryptor
	{
		static public int RSABit = 2048;

		public RSAKey privateRSAKey = new RSAKey();
		public RSAKey publicRSAKey = new RSAKey();

		public void GenerateRSAKeys()//2048-bit
		{
			//
			// Generate two random numbers
			//

			Generator.Initialize();

			// For 2048 bit key length, we take 1024 bits for first prime and 1024 for second prime
			// Get same min and max
			BigInteger numMin = BigInteger.Pow(2, (RSABit/2 - 1));
			BigInteger numMax = BigInteger.Pow(2, (RSABit/2));

			//Generating p and q
			// Create two prime numbers
			var p = new PrimeNumber();
			var q = new PrimeNumber();

			p.SetNumber(Generator.Random(numMin, numMin));
			q.SetNumber(Generator.Random(numMin, numMax));

			p.RabinMiller ();
			q.RabinMiller ();

			//Generating e and d
			BigInteger n = p.GetPrimeNumber () * q.GetPrimeNumber ();
			BigInteger euler = (p.GetPrimeNumber () - 1) * (q.GetPrimeNumber () - 1);

			var e = 65537;

			BigInteger d = MathExtended.ModularLinearEquationSolver(e, 1, euler);

			// N
			privateRSAKey.n = n;
			publicRSAKey.n = n;

			// E
			publicRSAKey.k = e;

			// D
			privateRSAKey.k = d;
		}

		private byte[] GenerateXORKey(int length)
		{
			Random rnd = new Random ();
			byte[] key = new byte[length];
			rnd.NextBytes (key);
			return key;
		}

		private string EncryptDecryptXOR(byte[] XORKey, string input, bool isDecrypting = false)
		{
			char[] inputCharArray = input.ToCharArray ();

			if (!isDecrypting && inputCharArray.Length % XORKey.Length != 0) {
				char[] _inputCharArray = new char[((int)(inputCharArray.Length/XORKey.Length)+1)*XORKey.Length];
				Random _r = new Random ();
				for (int i = 0; i < inputCharArray.Length; i++) {
					_inputCharArray [i] = inputCharArray [i];
				}
				_inputCharArray [inputCharArray.Length] = (char)255;
				for (int i = inputCharArray.Length + 1; i < _inputCharArray.Length; i++) {
					_inputCharArray [i] = (char)_r.Next (254);
				}
				inputCharArray = _inputCharArray;
			}

			char[] outputCharArray = new char[inputCharArray.Length];

			int j = 0;
			for (int i = 0; i < inputCharArray.Length; i++) {
				if (j >= XORKey.Length)
					j = 0;
				outputCharArray [i] = (char)(inputCharArray [i] ^ XORKey [j]);
				if (isDecrypting && outputCharArray [i] == 255) {
					outputCharArray [i] = (char)0;
					break;
				}
				j++;
			}
			String result = new string (outputCharArray);
			return result.Trim('\0');
		}

		public Packet EncryptPacket(RSAKey publicKey, string message)
		{
			byte[] XORKey = GenerateXORKey (RSABit/8);;
			while(new BigInteger(XORKey) > publicKey.n || (new BigInteger(XORKey)) < 0){
				XORKey = GenerateXORKey (RSABit/8);
			}
			Packet ret = new Packet ();
			ret.encryptedXORKey = EncryptRSA (publicKey, XORKey);
			ret.encryptedMessage = EncryptDecryptXOR (XORKey, message);
			return ret;
		}

		public string DecryptPacket(Packet packet)
		{
			byte[] XORKey = DecryptRSA (privateRSAKey, packet.encryptedXORKey);
			return EncryptDecryptXOR(XORKey,packet.encryptedMessage,true);
		}

		private BigInteger EncryptRSA(RSAKey key, byte[] XORKey)
		{
			BigInteger bigXORKey = new BigInteger (XORKey);
			bigXORKey = BigInteger.ModPow(bigXORKey,key.k,key.n);
			return bigXORKey;
		}

		private byte[] DecryptRSA(RSAKey key, BigInteger bigXORKey)
		{
			bigXORKey = BigInteger.ModPow(bigXORKey,key.k,key.n);
			return bigXORKey.ToByteArray ();
		}

	}
}

