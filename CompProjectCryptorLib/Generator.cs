﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Numerics;

namespace CompProjectCryptorLib
{
    static class Generator
    {
        private static Random _rnd = new Random();

        public static void Initialize ()
        {
            _rnd = new Random ();
        }

        public static BigInteger Random (BigInteger a, BigInteger b)
        {
            BigInteger retValue;

            BigInteger count = b - a;

            BigInteger digits = 0;

            while ( (count / 10) > 0)
            {
                count = count / 10;

                digits++;
            }

            string retVal = _rnd.Next (1000000000, 2100000000).ToString(CultureInfo.InvariantCulture);

            while (retVal.Length < digits)
            {
                retVal = retVal + _rnd.Next (1000).ToString(CultureInfo.InvariantCulture);
            }

            // We get the number, but we might be too high, so we do a mod
            retValue = BigInteger.Parse (retVal);

            retValue = a + (retValue % b);

            return retValue;
        }
    }
}
