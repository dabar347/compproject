﻿using NUnit.Framework;
using System;
using System.Reflection;
using CompProjectCryptorLib;

namespace CompProjectUnitTesting
{
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void XORKeyTest ()
		{
			Type type = typeof(Cryptor);
			var foo = Activator.CreateInstance (type);
			var testedMethod = foo.GetType ().GetMethod ("GenerateXORKey", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
			object[] testedParams = { 256 };
			byte[] key = (testedMethod.Invoke (foo, testedParams) as byte[]);
			Assert.True (key.Length == 256);
		}

		[Test ()]
		public void XOREncTest ()
		{
			Type type = typeof(Cryptor);
			var foo = Activator.CreateInstance (type);
			var generateXORKeyMethod = foo.GetType ().GetMethod ("GenerateXORKey", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
			object[] generateXORKeyMethodParams = { 256 };
			byte[] key = (generateXORKeyMethod.Invoke (foo, generateXORKeyMethodParams) as byte[]);
			String testString = "Test message";
			var testedMethod = foo.GetType ().GetMethod ("EncryptDecryptXOR", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
			object[] testedParams1 = { key, testString, false };
			string encryptedTestString = (string)testedMethod.Invoke(foo, testedParams1);
			object[] testedParams2 = {key, encryptedTestString, true};
			string decryptedTestString = (string)testedMethod.Invoke(foo, testedParams2);
			Assert.True(testString == decryptedTestString);
		}

		[Test ()]
		public void PacketEncTest ()
		{
			Cryptor foo = new Cryptor ();
			foo.GenerateRSAKeys ();
			String testMessage = "Test message";
			Packet encryptedTestMessage = foo.EncryptPacket (foo.publicRSAKey, testMessage);
			String decryptedTestMessage = foo.DecryptPacket (encryptedTestMessage);
			Assert.True (testMessage == decryptedTestMessage);
		}
	}
}

