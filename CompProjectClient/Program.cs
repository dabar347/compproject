using System;
using CompProjectCryptorLib;
using WebSocketSharp;
using System.Numerics;
using System.Collections.Generic;

namespace CompProjectClient
{
	class MainClass
	{
		static bool VERBOSE_MODE = true;

		public static class Log
		{
			public static void log(string str)
			{
				if (VERBOSE_MODE) {
					Console.WriteLine (str);
				}
			}
		}

		private struct UserEntry
		{
			public string username;
			public RSAKey key;

		}

		private static string clientState = "1";
		private static string clientInput = "";
		private static Cryptor cryptor = new Cryptor();

		private static string login = "";
		private static string password = "";
//		private static string token = "";
		private static List<UserEntry> userList = new List<UserEntry>();
		private static int nextRecipient = 0;

		static WebSocket ws = new WebSocket ("ws://localhost:48086/enchse");

		public static void TestRun()
		{
			double test_successes = 0;
			double test_messages_successes = 0;
			cryptor.GenerateRSAKeys ();
			Console.WriteLine ("=TEST RUN");
			Console.WriteLine ("");
			Console.WriteLine ("Public key");
			Console.WriteLine ("n = " + cryptor.publicRSAKey.n);
			Console.WriteLine ("k = " + cryptor.publicRSAKey.k);
			Console.WriteLine ("Private key");
			Console.WriteLine ("n = " + cryptor.privateRSAKey.n);
			Console.WriteLine ("k = " + cryptor.privateRSAKey.k);
			Console.WriteLine ("");
			for (int i = 0; i < 100; i++) {
				Console.WriteLine ("Trial: " + i);
				byte[] test_key = cryptor.GenerateXORKey (1024/8);
				while(new BigInteger(test_key) > cryptor.publicRSAKey.n || (new BigInteger(test_key)) < 0){
					test_key = cryptor.GenerateXORKey (1024/8);
				}
//				bool test_status = CompareXORKeys (test_key, (cryptor.decryptRSA(cryptor.privateRSAKey,cryptor.encryptRSA(cryptor.publicRSAKey,test_key))));
				BigInteger test_bigXORKey1 = new BigInteger (test_key);//ByteArray2BigInteger(XORKey);
				BigInteger test_bigXORKey2 = BigInteger.ModPow(test_bigXORKey1,cryptor.publicRSAKey.k,cryptor.publicRSAKey.n);
				BigInteger test_bigXORKey3 = BigInteger.ModPow(test_bigXORKey2,cryptor.privateRSAKey.k,cryptor.privateRSAKey.n);
//				bool test_status = ((test_bigXORKey1 != test_bigXORKey3) && (test_message1 == test_message2.Trim ('\0')) || 
//					(test_bigXORKey1 == test_bigXORKey3) && (test_message1 != test_message2.Trim ('\0')));
					Console.WriteLine ("XOR keys: 1: " + test_bigXORKey1);
					Console.WriteLine ("XOR keys: 2: " + test_bigXORKey2);
					Console.WriteLine ("XOR keys: 3: " + test_bigXORKey3);
				Console.WriteLine ("XOR keys: "+(test_bigXORKey1 == test_bigXORKey3));
				Console.WriteLine ("");
//				Console.WriteLine ("Trial: " + i);
				string test_message1 = "qwdkjnviufnIHKBKHBN<JNLKJNlcksvdfbdbdgbirgubj446623";
				Packet test_packet = cryptor.encryptPacket (test_key,cryptor.publicRSAKey, test_message1);
				string test_message2 = cryptor.DecryptPacket (test_packet);
				bool test_status = (test_message1 != test_message2.Trim ('\0')) && (test_bigXORKey1 != test_bigXORKey3);
				bool test_message_status = (test_message1 == test_message2.Trim ('\0'));
				test_successes += test_status ? 1 : 0;
				test_messages_successes += test_message_status ? 1 : 0;
				Console.WriteLine ("Message: "+(test_message1 == test_message2.Trim ('\0')));
				Console.WriteLine ("");
			}
			Console.WriteLine ("=END TEST RUN "+test_messages_successes+"/100 "+(100-test_successes).ToString()+"/100");
		}

		public static void Main (string[] args)
		{
			TestRun ();
//			cryptor.generateRSAKeys ();
//			Log.log ("Key generated");
//			Packet test = cryptor.encryptPacket (cryptor.publicRSAKey, "Лично мне так и не удалось осуществить звонок. Чаще всего по ссылке на звонок открывается чистое окно (вкладка).\nНа новом профиле открывается нечто похожее на окно со звонком, но ничего в принципе не слышно и меня никто не слышит");
//
//			Console.WriteLine (test.ToString ());
//			Packet test1 = new Packet ();
//			test1.FromString (test.ToString ());
//
//
//
////			test.FromString (test.ToString ());
//			Console.WriteLine (test1.ToString ());
//			 
//			Console.WriteLine (test.encryptedMessage);
//			String s = cryptor.decryptPacket (test);
//			Console.WriteLine(s);
//			Console.ReadKey ();//
//			ws.OnOpen += (sender, e) => 
//			{
//				Log.log("OnOpen");
//				ws.Send("HANDSHAKE!!");
//
//			};
//			ws.OnMessage += (sender, e) => 
//			{
//				Log.log("OnMessage");
//				Log.log(e.Data);
//				string[] data = e.Data.Split ('!');
//				//data = e.Data.Split ('!');
//				if (data [0].Length > 0) {
//					switch (data [0]) 
//					{
//					case "HANDSHAKE":
//						clientState = "1";
//						interactiveLoop();
//						ws.Send("LOGIN!"+login+"!"+password);
//						break;
//					case "UPDATE_TOKEN":
//						token = data[1];
//						ws.Send("UPDATE_KEY!"+login+"!"+token+"!"+Convert.ToString(cryptor.publicRSAKey.k)+"!"+Convert.ToString(cryptor.publicRSAKey.n));
//						break;
//					case "HANDSHAKE_DONE":
//						ws.Send("LIST");
//						break;
//					case "LIST":
//						//n = (l-1)/2
//						//1,2,3
//						userList = new List<UserEntry>();
//						for (var i = 0; i < (data.Length-1)/3; i++)
//						{
//							BigInteger _k = new BigInteger();
//							BigInteger _n = new BigInteger();
//							BigInteger.TryParse(data[3*i+2],out _k);
//							BigInteger.TryParse(data[3*i+3],out _n);
//							userList.Add(new UserEntry(){username = data[3*i+1], key = new RSAKey(){k = _k, n = _n}});
////							userList..username = data[3*i+1];
////							userList[i].key.k = new BigInteger(data[3*i+2]);
////							userList[i].key.n = new BigInteger(data[3*i+3]);
//						}
//						clientState = "3";
//						interactiveLoop();
//						if (clientState != "5")
//						{
//							ws.Send("LIST");
//						}
//						break;
//					default:
//						break;
//					}
//				} else {
//				}
//			};
//
//			ws.Connect ();
//			while (clientState != "5") {
//			}
//			ws.Close ();
		}

		static bool CompareXORKeys (byte[] xor1, byte[] xor2)
		{
			if (xor1.Length != xor2.Length) {
				Console.WriteLine ("XOR keys: length does not match");
				return false;
			}
			for (int i = 0; i < xor1.Length; i++) {
				if (xor1 [i] != xor2 [i]) {
					Console.WriteLine ("XOR keys: i = "+i);
					Console.WriteLine ("XOR keys: original byte = "+xor1[i]);
					Console.WriteLine ("XOR keys: original char = "+Convert.ToChar(xor1[i]));
					Console.WriteLine ("XOR keys: represented byte = "+xor2[i]);
					Console.WriteLine ("XOR keys: represented char = "+Convert.ToChar(xor2[i]));
					return false;
				}
			}
			return true;
		}

		static bool ComparePackets (Packet p1, Packet p2)
		{
			if (p1.encryptedMessage != p2.encryptedMessage) {
				Console.WriteLine ("Packets: p1.encryptedMessage" + p1.encryptedMessage);
				Console.WriteLine ("Packets: p2.encryptedMessage" + p2.encryptedMessage);
				return false;
			} else if (p1.encryptedXORKey != p2.encryptedXORKey) {
				Console.WriteLine ("Packets: p1.encryptedXORKey" + p1.encryptedXORKey);
				Console.WriteLine ("Packets: p2.encryptedXORKey" + p2.encryptedXORKey);
				return false;
			}
			return true;
		}

//		private static void TestRun(){
//			for (int j = 0; j < 200; j++) {
//				Console.WriteLine ("=TEST RUN "+j);
//
//				cryptor.generateRSAKeys ();
//				String test_message = "BOOBS";
//
//				Console.WriteLine ("Public key");
//				Console.WriteLine ("n = " + cryptor.publicRSAKey.n);
//				Console.WriteLine ("k = " + cryptor.publicRSAKey.k);
//				Console.WriteLine ("Private key");
//				Console.WriteLine ("n = " + cryptor.privateRSAKey.n);
//				Console.WriteLine ("k = " + cryptor.privateRSAKey.k);
//
//
//				for (int i = 0; i < 1000; i++) {
//					//Stage 1
//					byte[] stage1_xor_key = cryptor.generateXORKey (1024 / 8);
//					String stage1_enc_message = cryptor.encryptDecryptXOR (stage1_xor_key, test_message);
//					//Stage 2
//					Packet stage2_packet = cryptor.encryptPacket (stage1_xor_key, cryptor.publicRSAKey, test_message);
//					//Stage 3
//					String stage3_base64_packet = stage2_packet.ToString ();
//					//Stage 7
//					Packet stage7_packet = new Packet ();
//					stage7_packet.FromString (stage3_base64_packet);
//					//Stage 8
//					byte[] stage8_xor_key = cryptor.decryptRSA (cryptor.privateRSAKey, stage7_packet.encryptedXORKey);
//					//Stage 9
//					string stage9_message = cryptor.decryptPacket (stage7_packet);
//					stage9_message = stage9_message.Trim ('\0');
//
//					Console.WriteLine ("Trial: " + i);
//					Console.WriteLine ("Message: " + (test_message == stage9_message));
//					Console.WriteLine ("XOR keys: " + CompareXORKeys (stage1_xor_key, stage8_xor_key));
//					Console.WriteLine ("Packets: " + ComparePackets (stage2_packet, stage7_packet));
//					Console.WriteLine ("");
//
//				}
//				Console.WriteLine ("=END TEST RUN");
//			}
////			Console.WriteLine ("TEST RUN");
////
//
////			Cryptor testCryptor = new Cryptor ();
////			for (int x = 0; x < 10; x++) {
////				Console.WriteLine ("Cryptor: " + x);
////				testCryptor.generateRSAKeys ();
//////				Console.WriteLine ("");
//////				Console.WriteLine ("Private key:");
//////				Console.WriteLine ("k = " + testCryptor.privateRSAKey.k);
//////				Console.WriteLine ("n = " + testCryptor.privateRSAKey.n);
//////				Console.WriteLine ("");
//////				Console.WriteLine ("Public key:");
//////				Console.WriteLine ("k = " + testCryptor.publicRSAKey.k);
//////				Console.WriteLine ("n = " + testCryptor.publicRSAKey.n);
////				RSAKey reproducedPublicKey = new RSAKey ();
////				reproducedPublicKey.FromString (testCryptor.publicRSAKey.ToString ());
//////				Console.WriteLine ("");
//////				Console.WriteLine ("Reproduced public key:");
//////				Console.WriteLine ("k = " + reproducedPublicKey.k);
//////				Console.WriteLine ("n = " + reproducedPublicKey.n);
////				for (int i = 0; i < 100; i++) {
////					Console.WriteLine ("");
////					Console.WriteLine ("Test message: " + i);
////					Packet testPacket = testCryptor.encryptPacket (reproducedPublicKey, "TEST");
////					Console.WriteLine ("");
////					Console.WriteLine ("Packet: " + testPacket.ToString ());
////					Console.WriteLine ("Encrypted key: " + testPacket.encryptedXORKey);
////					Console.WriteLine ("Encrypted message: " + testPacket.encryptedMessage);
////					Packet reproducedPacket = new Packet ();
////					reproducedPacket.FromString (testPacket.ToString ());
////					Console.WriteLine ("");
////					Console.WriteLine ("Reproduced Packet: " + reproducedPacket.ToString ());
////					Console.WriteLine ("Reproduced Encrypted key: " + reproducedPacket.encryptedXORKey);
////					Console.WriteLine ("Reproduced Encrypted message: " + reproducedPacket.encryptedMessage);
////					Console.WriteLine ("");
////					Console.WriteLine ("Decrypted message: " + testCryptor.decryptPacket (reproducedPacket));
////					Console.WriteLine ("Status: " + (testCryptor.decryptPacket (reproducedPacket) == "TEST"));
////				}
////			}
////
////			Console.WriteLine ("TEST RUN END");
//		}

		private static void interactiveLoop()
		{
			while (clientState != "5" && clientState != "") {
				//Do pre-actions
				preActions ();
				//Wait
				clientInput = Console.ReadLine ();
				//Do post-actions
				postActions ();
				//Change state
				changeState ();
			}
		}

		private static void preActions()
		{
			switch (clientState) {
			case "1":
				Console.WriteLine ("Enter login");
				Console.Write ("> ");
				break;
			case "2":
				Console.WriteLine ("Enter password");
				Console.Write ("> ");
				break;
			case "3":
				Console.WriteLine ("0: Refresh list");
				int i = 1;
				foreach (var v in userList) {
					Console.WriteLine (i + ": " + v.username);
					i++;
				}
				Console.Write ("> ");
				break;
			case "4":
				Console.WriteLine ("Type Message");
				Console.Write ("> ");
				break;
			}
		}

		private static void postActions()
		{
			switch (clientState) {
			case "1":
				login = clientInput;
				break;
			case "2":
				password = clientInput;
				break;
			case "3":
				nextRecipient = Convert.ToInt32 (clientInput) - 1;
				break;
			}
		}

		private static void changeState()
		{
			switch (clientState) {
			case "1":
				if (login != "")
					clientState = "2";
				break;
			case "2":
				if (password != "")
					clientState = "";
				break;
			case "3":
				if (clientInput == "0")
					clientState = ""; 
				else {
					clientState = "4";
				}
				break;
			case "4":
				if (clientInput != "") {
					Packet p = cryptor.encryptPacket (userList [nextRecipient].key, clientInput);
					//Log.log ("MESSAGE!" + userList [nextRecipient].username + "!" + p.ToString ());
					ws.Send ("MESSAGE!" + userList [nextRecipient].username + "!" + p.ToString ());
					clientState = "3";
				}
				break;
			}
		}

	}
}
