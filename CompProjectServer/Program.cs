using System;
using WebSocketSharp;
using WebSocketSharp.Server;
using Mono.Data.Sqlite;
using System.IO;
using System.Reflection;
using CompProjectCryptorLib;
using System.Data;
using System.Collections.Generic;

namespace CompProjectServer
{
	class MainClass
	{
		//Environmental vars
		public static bool VERBOSE_MODE = true;
		public static string LINK = "enchse";


		//SQLite vars
		static string connectionString = "URI=file:" + Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "/" + LINK + ".db";


		public static class Log
		{
			public static void log(string str)
			{
				if (VERBOSE_MODE) {
					Console.WriteLine (str);
				}
			}
		}

		static WebSocketServer wss;

		private static object doQuery(string command, bool complicated = false)
		{
			object result;
			using (SqliteConnection _connection = new SqliteConnection (connectionString)) {
				_connection.Open ();
				using (SqliteCommand _command = new SqliteCommand (_connection)) {
					_command.CommandText = command;
					if (complicated) {
						result = _command.ExecuteReader();
					} else {
						result = _command.ExecuteScalar();
					}
					Log.log	(Convert.ToString (result));
				}
				_connection.Close ();
			}
			return result;
		}

		private static string prepareListResponse()
		{	
			string result = "LIST";
			using (SqliteConnection _connection = new SqliteConnection (connectionString)) {
				_connection.Open ();
				using (SqliteCommand _command = new SqliteCommand (_connection)) {
					_command.CommandText = @"SELECT Username, PublicK, PublicN FROM ConnectedUsers";
					IDataReader reader = _command.ExecuteReader ();
					while (reader.Read ()) {
						for (var i = 0; i < 3; i++)
						{
							result += "!" + reader.GetString (i);
						};
					}
					Log.log	(Convert.ToString (result));
				}
				_connection.Close ();
			}
			return result;

		}

		public static void Main (string[] args)
		{
			//Set up environment

			//DB
			Log.log ("Preparing DB...");
			//Log.log	("SQLite version : {0}", doQuery("SELECT SQLITE_VERSION()"));
			if (Convert.ToString(doQuery (@"SELECT name FROM sqlite_master WHERE type='table' AND name='Users'")) != "Users") {
				doQuery (@"CREATE TABLE Users(Id INTEGER PRIMARY KEY, Username TEXT, Password TEXT, Token TEXT, TokenExpires TEXT)");
			}
			if (Convert.ToString(doQuery (@"SELECT name FROM sqlite_master WHERE type='table' AND name='ConnectedUsers'")) == "ConnectedUsers") {
				doQuery (@"DROP TABLE ConnectedUsers");
			}
			doQuery (@"CREATE TABLE ConnectedUsers(Id INTEGER PRIMARY KEY, Username TEXT, PublicK TEXT, PublicN TEXT, SessionID TEXT)");
			//string list = prepareListResponse ();
			Log.log ("DB prepared and loaded");
			//WebSocket
			Log.log ("Server starting...");
			wss = new WebSocketServer (48086);
			wss.Log.Level = VERBOSE_MODE ? LogLevel.Trace : wss.Log.Level;
			wss.AddWebSocketService<Service> ("/"+LINK);
			wss.Start ();
			Log.log ("Server started");
			Console.ReadKey (true);
			wss.Stop ();
		}



		class Service : WebSocketService
		{

			private string _sessionName = "";
			public string sessionName {
				get
				{
					return _sessionName;
				}
				set 
				{
					if (_sessionName != "") {
						//TODO throw exception
					} else {
						_sessionName = value;
					}
				}
			}

			string generateToken()
			{
				string r;
				do {
					Random rnd = new Random ();
					byte[] ba = new byte[32];
					rnd.NextBytes (ba);
					r = BitConverter.ToString(ba);
				} while(Convert.ToString (doQuery (@"SELECT Token FROM Users WHERE Username='" + r + "'")) != "");
				return r;
			}

			bool checkToken(string Username, string Token)
			{
				//doQuery (@"SELECT TokenExpires FROM Users WHERE Username='" + Username + "'");
				return (Convert.ToString (doQuery (@"SELECT Token FROM Users WHERE Username='" + Username + "'")) == Token &&
					Convert.ToDouble (doQuery (@"SELECT TokenExpires FROM Users WHERE Username='" + Username + "'")) > (DateTime.UtcNow - new DateTime (1970, 1, 1, 0, 0, 0)).TotalSeconds);
			}

			protected override void OnMessage (MessageEventArgs e)
			{
				//Command!Data
				//Command - numerical, e.g. 1.1 for the Handshake request
				//Data:
				//1 - Pure data
				//2 - Encrypted_XOR_key!Encrypted_data
				//Log.log ("Recieved:" + e.Data);
				//this.Log.Level = LogLevel.Trace;
				string[] data = new string[5];
				string qResult;
				//Console.WriteLine (e.Data);
				data = e.Data.Split ('!');
				if (data [0].Length > 0) {
					switch (data [0]) 
					{
					case "HANDSHAKE":
						Send ("HANDSHAKE!!");
						break;
					case "LOGIN":
						qResult = Convert.ToString (doQuery (@"SELECT Password FROM Users WHERE Username='" + data [1] + "'"));
						string token = generateToken ();
						if (qResult == "") {
							doQuery (@"INSERT INTO Users (Username, Password, Token, TokenExpires) VALUES ('" + data [1] + "','" + data [2] + "','"+token+"',"+Convert.ToString((DateTime.UtcNow.AddDays(1) - new DateTime(1970,1,1,0,0,0)).TotalSeconds)+");");
							Send ("UPDATE_TOKEN!" + token);
							sessionName = data [1];
						} else if (qResult == data [2]) {
							doQuery (@"UPDATE Users SET Token = '"+token+"' , TokenExpires = '"+Convert.ToString((DateTime.UtcNow.AddDays(1) - new DateTime(1970,1,1,0,0,0)).TotalSeconds)+"' WHERE Username='" + data [1] + "'");
							Send ("UPDATE_TOKEN!" + token);
							sessionName = data [1];
						} else {
							Send ("ERROR!!");
						}
						break;
					case "UPDATE_KEY":
						if (checkToken(data[1],data[2])){
							qResult = Convert.ToString (doQuery (@"SELECT Username FROM ConnectedUsers WHERE Username='" + data [1] + "'"));
							if (qResult == "") {
								doQuery (@"INSERT INTO ConnectedUsers (Username, PublicK, PublicN, SessionID) VALUES ('" + data [1] + "','" + data [3] + "','" + data [4] + "','" + this.ID + "');");
							} else if (qResult == data [1]) {
								doQuery (@"UPDATE ConnectedUsers SET PublicK = '"+data[3]+"', PublicN = '"+data[4]+"', SessionID = '"+this.ID+"' WHERE Username = '" + data[1] + "'");
							}
							Send ("HANDSHAKE_DONE");
						}
						break;
					case "LIST":
						Send (prepareListResponse ());
						break;
					case "MESSAGE":
						Console.WriteLine ("MESSAGE!" + this.sessionName + "!" + data [2]);
						Sessions.Broadcast ("MESSAGE!" + this.sessionName + "!" + data [2]);
//						string _id = "";
//						IWebSocketSession _s = null;
//						foreach (IWebSocketSession v in Sessions.Sessions) {
//							if (data [1] == (v as Service).sessionName) {
//								_id = v.ID;
//								_s = v;
//								break;
//							}
//						}
//						Console.WriteLine (_id);
//						if (_id != "") {
//							_s.Context.WebSocket.Send ("MESSAGE!" + this.sessionName + "!" + data [2]);
//							//Sessions.SendTo (_id, "MESSAGE!" + this.sessionName + "!" + data [2]);
//						} else {
//							Send ("ERROR!!");
//						}
						break;
					default: 
						Send ("ERROR!!");
						break;
					}
				} else {
					Send ("Wrong packet");
				}
			}
		}
	}
}
