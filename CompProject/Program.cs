using System;
using System.Collections.Generic;
using Gtk;

using Mono.Math;

namespace CompProject
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();

			//UI part
			Fixed _fixed = new Fixed ();
			win.Add (_fixed);

			//Login screen
			Button loginButton = new Button ();

			_fixed.Add (loginButton);
			win.ShowAll ();

			WebSocketClient wsc = new WebSocketClient ();


//			Cryptor crp = new Cryptor ();
//			crp.generateRSAKeys ();
//			byte[] r = crp.decryptRSA(crp.privateRSAKey, crp.encryptRSA (crp.publicRSAKey, crp.generateXORKey(4)));
//			Console.WriteLine ("");
			int t = DateTime.Now.Millisecond;
			Cryptor crp = new Cryptor ();
			do {
				crp.generateRSAKeys ();
			} while  (!crp.keysInternalTest ());
			Packet test = crp.encryptPacket (crp.publicRSAKey, "Hello");
			string enc = crp.decryptPacket (test);
			Console.WriteLine (enc);
			Console.WriteLine ("Time spent: " + (DateTime.Now.Millisecond - t));

			//Console.WriteLine (primes [primes.Count - 1] * primes [primes.Count - 1] > UInt32.MaxValue);
			/*crp.generateXORKey (1024);
			string test = crp.encriptXOR ("Xamarin is prioritizing updating our components so that you can start using them in your Unified API iOS apps, as well as updating the iOS Designer custom control renderer.\n\nOur recommendation therefore is this: if you don’t need to utilize the App Extensions framework today, hold off on moving to our preview Unified API until the toolchain and components have caught up.\n\nThanks!");
			Console.WriteLine (test);
			Console.WriteLine( crp.encriptXOR( test ) );*/

			Application.Run ();
		}
	}
}
